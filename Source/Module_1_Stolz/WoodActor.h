// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "WoodActor.generated.h"

UCLASS()
class MODULE_1_STOLZ_API AWoodActor : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AWoodActor();

private:
	// FOW texture size
	static const int Size = 512;
	UPROPERTY()
		UStaticMeshComponent * mesh;
	UPROPERTY()
		UTexture2D * texture;
	UPROPERTY()
		UMaterialInterface * material;
	UPROPERTY()
		UMaterialInstanceDynamic * MaterialInstance;

	uint8 m_pixelArray[Size * Size]; // Array the size of the texture
	FUpdateTextureRegion2D Region;

	float coverSize;
};